package com.epam.autocode.demo.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests implemented by student
 * <p>
 * You can prepare a template class like this CalculatorTests
 * as well as one empty method with the appropriate annotations like
 * {@code @Test // make sure it's from org.junit.jupiter.api package }
 * {@code @DisplayName("Human-readable test case name"))
 */
class CalculatorTests {

    @Test
    @DisplayName("Calculator adds two numbers and returns correct result")
    void add() {
        var calculator = new Calculator();

        var result = calculator.add(10, 5);

        assertEquals(15, result);
    }

    @Test
    @DisplayName("Calculator subtracts two numbers and returns correct result")
    void subtract() {
        var calculator = new Calculator();

        var result = calculator.subtract(99, 9);

        assertEquals(90, result);
    }

    @Test
    @DisplayName("Calculator multiplies two numbers and returns correct result")
    void multiply() {
        var calculator = new Calculator();

        var result = calculator.multiply(30, 3);

        assertEquals(90, result);
    }

    @Test
    @DisplayName("Calculator divides two numbers and returns correct result")
    void divide() {
        // Demo example: Student forgets to cover this method
    }
}